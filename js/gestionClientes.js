var clientesObtenidos;

function getClientes() {

  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request= new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState ==4 && this.status ==200){
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();

    }
  }

  request.open("GET",url,true);
  request.send();
}

function procesarClientes() {

  var JSONClientes = JSON.parse(clientesObtenidos);
  var urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tBody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped")

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevafila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if (JSONClientes.value[i].Country=="UK") {
      imgBandera.src = urlBandera+"United-Kingdom.png";
    }
      else {
        imgBandera.src = urlBandera+JSONClientes.value[i].Country+".png";
      }


    nuevafila.appendChild(columnaNombre);
    nuevafila.appendChild(columnaCiudad);

    nuevafila.appendChild(columnaBandera);
    columnaBandera.appendChild(imgBandera);

    tBody.appendChild(nuevafila);

  }
  tabla.appendChild(tBody);
  divTabla.appendChild(tabla);

}
